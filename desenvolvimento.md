#Etapas de Desenvolvimento

1. Primeiramente desenvolvi todas as camadas da páginas e o posicionamento básico do conteúdo, título, textos, divisórias, etc.

2. Depois encaixei as illustrações.

    21. Algumas imagens continham uma área maior do que realmente era exibido de informação, decidi diminuir as imagens com o photoshop e comprimi-las online para melhor trabalhá-las sem perder a qualidade, apenas removendo o desnecessário.

    22. Não consegui encontrar na pasta uma imagem que estava presente no projeto visual, então decidi fazê-la para inserir no teste (a imagem encontra-se em assets/images/tracos-bolas.png).

3. Por fim tratei a responsividade do site. A página fica exatamente como o projeto visual na resolução 1920x1080, adaptando-se as demais, funcionando até em monitores ultra-wide sem perder a qualidade. As ilustrações, cujo objetivo era apenas embelezar a página, foram removidas em resoluções mobile para deixar a página mais leve na hora de carregar nos smartphones em conexao 3g.

#Observações
    - Para a confecção do teste utilizei bootstrap para montar a base do site.
    - Como era requisitado, procurei aprender algum pré-processador css, pelo menos o básico de sass para o desenvolvimento. Embora ja tivesse ouvido falar na ferramenta, nunca havia a utilizado e o teste me serviu como gatilho para procurar sobre.
    - Em relação as imagens ".svg" notei que uma mesma imagem era utilizada duas vezes, porém de cores diferentes. Tentei alterar as cores dinâmicamente, mas não encontrei nada sobre. Por fim decidi apenas copiar a imagem e alterar o fill dela no arquivo.
    - No CSS por alguns padrões pessoais de desenvolvimento, gosto de criar algumas divisões no arquivo para deixar configurações sobre determinados itens próximas e de fácil acesso, como: TEXTO, BOTÕES, THUMBS, etc.
    - Os icones de rede social eu baixei do site "font-awesome" geralmente o utilizo para posicionar icones em geral no site, porém como os icones de redes sociais só estão disponiveis nas versões pagas eu baixei por la.

#Considerações Finais
    É a primeira vez que construo uma página cujo design não tenha sido planejado por mim, de forma geral não senti muitas dificuldades, mas ainda sim foi um desafio construir algo diferente do que, até o momento, estava acustumado a fazer e por ter uma identidade visual mais profissional.

    